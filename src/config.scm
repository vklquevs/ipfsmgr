
(define (read-config-file fname)
  (with-input-from-file fname read))
(define (write-config-file fname cfg)
  (unless (*dry-run*)
    (unless (*quiet*) (warn "Writing config file ~a" fname))
    (let ((port (open-output-file fname)))
      (pretty-print cfg port)
      (close port))
    (unless (*quiet*) (warn ".\n"))))

(define*
  (config-file-contents
    repos
    #:key daemon-dir repo-dir block-dir)
  `((config
      (daemon-dir ,daemon-dir)
      (repo-dir ,repo-dir)
      (block-dir ,block-dir))
    (repos)))
(define (config-file-get cfg path)
  (match
    path
    ((key) (assq-ref cfg key))
    ((key . tail)
     (config-file-get (assq-ref cfg key) tail))))

(define (config-file-set cfg path val)
  (define (norm al) (hash-table->alist (alist->hash-table al)))
  (match
    path
    ((key)
     (norm (acons key val cfg)))
    ((key . tail)
     (let ((v (assq-ref cfg key)))
       (norm (acons key (config-file-set v tail val) cfg))))))

(define (config-file-repo cfg name)
  (let* ((r (config-file-get cfg `(repos ,(string->symbol name))))
         (repo-dir (apply-or car (config-file-get r '(repo-dir))))
         (daemon-dir (apply-or car (config-file-get r '(daemon-dir))))
         (gateway-port (apply-or car (config-file-get r '(gateway-port))))
         (api-port (apply-or car (config-file-get r '(api-port)))))
    (and r (make-repo
             #:repo-dir repo-dir #:daemon-dir daemon-dir
             #:name name
             #:api-port api-port
             #:gateway-port gateway-port
             #:base-dirs? #f))))
(define (config-file-repos cfg)
  (let* ((repos (config-file-get cfg '(repos))))
    (map (lambda (r)
           (config-file-repo cfg (symbol->string (car r))))
         repos)))

(define
  (config-file-update-repo
    cfg repo)
  (config-file-set cfg
                   `(repos ,(string->symbol (repo-name repo)))
                   (cdr (repo->list repo))))

