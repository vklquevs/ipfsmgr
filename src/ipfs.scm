
(define*
  (ipfs-run repo cmd args #:key read? quiet? force?)
  (run*
    (cons* "ipfs" "-c" (repo-repo-dir repo) cmd args)
    #:read? read? #:quiet? quiet? #:force? force?))

(define (ipfs-set-ports repo)
  (define (set-port key port)
    (let* ((this-addr
             (string-trim-both
               (ipfs-run repo "config" (list key)
                         #:read? #t #:quiet? #t #:force? #t)))
           (last-/ (string-rindex this-addr #\/))
           (this-root (string-take this-addr last-/))
           (new-addr (format #f "~a/~a" this-root port)))
      (ipfs-run repo "config" (list key new-addr))))
  (set-port "Addresses.API" (repo-api-port repo))
  (set-port "Addresses.Gateway" (repo-gateway-port repo)))

(define (ipfs-link-block-dir repo block-dir)
  (let ((block (file-path (repo-repo-dir repo) "blocks")))
  (for-each
    (lambda (x) (apply run x))
    `(("mkdir" "-p" ,block-dir)
        ("rm" "-r" ,block)
        ("ln" "-s" ,block-dir ,block)))))

(define (ipfs-init repo)
  (ipfs-run repo "init" '("-e")))

