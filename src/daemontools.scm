
(define (daemontools-directory? dir)
  (file-exists? (file-path dir "run")))
(define (daemontools-down? dir)
  (file-exists? (file-path dir "down")))

(define (daemontools-status dir)
  (cond
    ((not (file-exists? dir)) #f)
    ((not (daemontools-directory? dir)) 'invalid)
    ((daemontools-down? dir) 'down)
    (#t #t)))

(define (ensure-daemontools-directory dir)
  (unless
    (daemontools-directory? dir)
    (warn "Aborting: '~a' doesn't look like a daemontools directory.\n"
          dir)
    (quit 1)))

(define (daemontools-init dir script)
  (when
    (and (not (*force*))
         (file-exists? dir))
    (warn "Aborting: '~a' already exists. Use --force to overwrite.\n"
          dir)
    (quit 1))
  (run "mkdir" "-p" dir)
  (run "touch" (file-path dir "down"))
  (unless (*dry-run*)
  (with-output-to-file (file-path dir "run")
                       (lambda ()
                         (display script)
                         (newline))))
  (run "chmod" "+x" (file-path dir "run")))

(define (daemontools-start dir)
  (ensure-daemontools-directory dir)
  (when
    (and (not (*force*))
         (not (daemontools-down? dir)))
    (warn
      "Aborting: '~a' might already be running. Use --force to do it anyway.\n"
      dir)
    (quit 1))
  (run "rm" (file-path dir "down"))
  (run "svc" "-u" dir))

(define (daemontools-stop dir)
  (ensure-daemontools-directory dir)
  (when
    (and (not (*force*))
         (daemontools-down? dir))
    (warn
      "Aborting: '~a' might already be stopped. Use --force to do it anyway.\n"
      dir))
  (run "svc" "-d" dir)
  (run "touch" (file-path dir "down")))

