
(define*
  (show-help
    #:key quit? section abort)
  (define commands
    '(;("config"
      ; ("  ipfsmgr config\n"
      ;  "          Save a new configuration file or update existing config\n"
      ;  "    Extra arguments for config:\n"
      ;  "      -d, --daemon-dir  Base directory for daemons\n"
      ;  "      -r, --repo-dir    Base directory for IPFS repositories\n"
      ;  "      -b, --block-dir   Blocks directory for IPFS data deduplication\n"
      ;  "      -B, --no-dedup    Stop using a deduplication directory\n"))
      ("ls"
       ("  ipfsmgr ls\n"
        "          List all managed nodes.\n"))
      ("init"
       ("  ipfsmgr init REPO\n"
        "          Initialize a node named REPO.\n"
        "    Extra arguments for init:\n"
        "      -d, --daemon-dir  Location for daemon directory\n"
        "      -r, --repo-dir    Location for repository directory\n"
        "      -b, --block-dir   Blocks directory for IPFS data deduplication\n"
        "      -B, --no-dedup    Don't use blocks directory for this node\n"))
      ("apply"
       ("  ipfsmgr apply\n"
        "          Update configuration values from config file\n"
        "    Fields applied:\n"
        "      api-port     ipfs config Addresses.API\n"
        "      gateway-port ipfs config Addresses.Gateway\n"))
      ("enable"
       ("  ipfsmgr enable REPO\n"
        "          Start the daemon for REPO.\n"))
      ("disable"
       ("  ipfsmgr disable REPO\n"
        "          Stop the daemon for REPO.\n"))
      ("run"
       ("  ipfsmgr run REPO CMD [ARGS...]\n"
        "          Run IPFS command on this node.\n"
        "          Equivalent to `ipfs -c path/to/REPO CMD [ARGS...]`\n"))
      ("do"
       ("  ipfsmgr do CMD [ARGS...]\n"
        "          Run IPFS command on each node in turn.\n"))
      ))
  (display
    "ipfsmgr - Manage a bunch of IPFS nodes as daemontools services\n")
  (for-each
    display
    '("  -c, --config      Configuration file\n"
      "    Default: IPFSMGR_CONFIG_FILE or .ipfsmgrrc\n"
      "  -f, --force       Run all commands, even if unnecessary\n"
      "  -q, --quiet       Output less information\n"
      "  -n, --dry-run     Don't do anything\n"
      "  -h, --help        Show short help text\n"))
  (when section
    (display "Command help:\n")
    (if (eq? section #t)
      (for-each display (map caadr commands))
      (for-each display (cadr (assoc section commands)))))
  (when abort
    (format #t "Aborting (~a).\n" abort))
  (when (or abort quit?) (quit 9)))

(define *args*
  '((config (single-char #\c) (value #t))
    (daemon-dir (single-char #\d) (value #t))
    (repo-dir (single-char #\r) (value #t))
    (block-dir (single-char #\b) (value #t))
    (no-dedup (single-char #\B))
    (dry-run (single-char #\n))
    (quiet (single-char #\q))
    (force (single-char #\f))
    (help (single-char #\h) (value #f))))

(define (getargs args) (getopt-long args *args*))

(define (required-args . args)
  (format #f
          "Arguments required: ~a"
          (string-join (map (lambda (s)
                              (string-append "--" (symbol->string s)))
                            args)
                       ", ")))

(define (config-file-path opts)
  (or (option-ref opts 'config #f)
      (get-environment-variable "IPFSMGR_CONFIG_FILE")
      ".ipfsmgrrc"))

(define *quiet* (make-parameter #f))
(define *force* (make-parameter #f))
(define *dry-run* (make-parameter #f))

