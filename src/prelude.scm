#!/usr/bin/guile \
-e main -s
!#

(use-modules
  (srfi srfi-1) ; lists
  (srfi srfi-9) ; records
  (srfi srfi-9 gnu) ; immutable records
  (srfi srfi-69) ; hash-table
  (srfi srfi-98) ; env
  (ice-9 pretty-print)
  (ice-9 popen)
  (ice-9 rdelim)
  (ice-9 ftw)
  (ice-9 format)
  (ice-9 match)
  (ice-9 regex)
  (ice-9 getopt-long))

(define (warn . msg) (apply format (current-error-port) msg))
(define (abort fmt . msg)
  (apply warn (format #f "Aborting: ~a.\n" fmt) msg)
  (quit 1))

(define*
  (run* args #:key quiet? read? force?)
  (unless (or quiet? (*quiet*))
    (display (string-join (cons* ">" args)))
    (newline))
  (cond
    ((and (not force?) (*dry-run*)) #f)
    (read?
      (let* ((p (apply open-pipe* OPEN_READ args))
             (stdout (read-string p))
             (exitval (status:exit-val (close-pipe p))))
        (unless (eq? 0 exitval 0)
          (warn "`~a` failed with exit code ~a.\n"
                (string-join args) exitval)
          (exit exitval))
        stdout))
    (#t
     (let ((exitval (status:exit-val (apply system* args))))
       (unless (eq? 0 exitval)
         (warn "`~a` failed with exit code ~a.\n"
               (string-join args) exitval)
         (exit exitval))))))

(define (run . args)
  (run* args))

(define (file-path . args)
  (string-join args file-name-separator-string))

(define (absolute-file-path path)
  (if (absolute-file-name? path)
    path
    (file-path (getcwd) path)))

(define (apply-or fn v) (and v (fn v)))

