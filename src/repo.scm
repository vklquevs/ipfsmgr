
(define-immutable-record-type
  <repo>
  (new-repo name repo-dir daemon-dir gateway-port api-port)
  repo?
  (name repo-name)
  (repo-dir repo-repo-dir set-repo-repo-dir)
  (daemon-dir repo-daemon-dir set-repo-daemon-dir)
  (gateway-port repo-gateway-port)
  (api-port repo-api-port))

(define*
  (make-repo
    #:key name
    repo-dir daemon-dir
    base-dirs?
    gateway-port api-port
    (suffix ""))
  (new-repo
    name
    (string-append (if base-dirs? (file-path repo-dir name) repo-dir) suffix)
    (string-append (if base-dirs? (file-path daemon-dir name) daemon-dir) suffix)
    gateway-port api-port))

(define (repo->list repo)
  `(,(repo-name repo)
     (repo-dir ,(repo-repo-dir repo))
     (daemon-dir ,(repo-daemon-dir repo))
     (gateway-port ,(repo-gateway-port repo))
     (api-port ,(repo-api-port repo))))

(define (next-api-port repos first)
  (1+ (reduce max first (map repo-api-port repos))))

(define (next-gateway-port repos first)
  (1+ (reduce max first (map repo-gateway-port repos))))

(define (set-repo-defaults repo repo-dir daemon-dir suffix)
  (let* ((name (repo-name repo))
         (repo (set-repo-repo-dir
                 repo
                 (or (repo-repo-dir repo)
                     (string-append (file-path repo-dir name) suffix))))
         (repo (set-repo-daemon-dir
                 repo
                 (or (repo-daemon-dir repo)
                     (string-append (file-path daemon-dir name) suffix)))))
    repo))


