
(define (ls-config daemon-dir cfg)
  (define (status->string s)
    (case s
      ((#f)         "down")
      ((invalid)    "ERR ")
      ((down)       "down")
      ((#t)         "up  ")
      (else         "??? ")))
  (let ((repos
          (map (lambda (r)
                 (cons r (daemontools-status (repo-daemon-dir r))))
               (config-file-repos cfg))))
    (for-each
      (lambda (r)
        (let ((name (repo-name (car r))))
          (format #t "[~a]\n" name)
          (format #t "  ~a /ipns/~a\n"
                  (status->string (cdr r))
                  (string-trim-right
                    (ipfs-run (car r)
                              ; "name" '("resolve")
                              "config" '("Identity.PeerID")
                              #:read? #t #:quiet? #t)))))
      repos)
    (format #t "~a found.\n" (length repos))))

(define (ensure-ipfs-daemon repo)
  (unless (daemontools-directory? (repo-daemon-dir repo))
    (daemontools-init
      (repo-daemon-dir repo)
      (format #f "#!/bin/sh\nexec ipfs -c ~a daemon 2>&1\n"
              (absolute-file-path (repo-repo-dir repo))))))

(define*
  (ipfsmgr
    args
    #:key
    config
    config-file
    arg-repo-dir
    cfg-repo-dir
    arg-daemon-dir
    cfg-daemon-dir
    arg-block-dir
    cfg-block-dir
    arg-no-dedup
    help?)
  (when help?
    (if (null? args)
      (show-help #:quit? #t)
      (show-help #:quit? #t #:section (car args))))
  (match
    args
    (("ls")
     (ls-config
       (or arg-daemon-dir cfg-daemon-dir
           (abort (required-args 'daemon-dir)))
       config))
    (("ls" . _)
     (show-help #:quit? #t #:section "ls" #:abort "Too many arguments"))
    (("init" name)
     (let* ((daemon-directory
              (cond
                (arg-daemon-dir arg-daemon-dir)
                (cfg-daemon-dir
                  (string-append (file-path cfg-daemon-dir name) ".ipfs"))
                (#t #f)))
            (repo-directory
              (cond
                (arg-repo-dir arg-repo-dir)
                (cfg-repo-dir
                  (string-append (file-path cfg-repo-dir name) ".ipfs"))
                (#t #f)))
            (block-directory (or arg-block-dir cfg-block-dir))
            (_ (unless (and daemon-directory repo-directory
                            (or arg-no-dedup block-directory))
                 (show-help #:quit? #t #:section "init"
                            #:abort
                            (apply required-args
                                   (if arg-no-dedup
                                     '(daemon-dir repo-dir)
                                     '(daemon-dir repo-dir block-dir))))))
            (repo (make-repo
                    #:name name
                    #:repo-dir repo-directory
                    #:daemon-dir daemon-directory
                    #:base-dirs? #f
                    #:gateway-port
                    (next-gateway-port (config-file-repos config) 6000)
                    #:api-port
                    (next-api-port (config-file-repos config) 5000))))
       (when
         (and (not (*force*))
              (or (config-file-repo config name)
                  (file-exists? repo-directory)
                  (file-exists? daemon-directory)))
         (abort "'~a' already exists. Use --force to overwrite" name))
       (ipfs-init repo)
       (ipfs-set-ports repo)
       (unless arg-no-dedup
         (ipfs-link-block-dir repo block-directory))
       (ensure-ipfs-daemon repo)
       (write-config-file
         config-file
         (config-file-update-repo config repo))))
    (("init" . _)
     (show-help #:quit? #t #:section "init" #:abort "One argument required"))
    (("enable" name)
     (let ((repo (config-file-repo config name)))
       (ensure-ipfs-daemon repo)
       (daemontools-start (repo-daemon-dir repo))))
    (("enable" . _)
     (show-help #:quit? #t #:section "enable" #:abort "One argument required"))
    (("disable" name)
     (let ((repo (config-file-repo config name)))
       (daemontools-stop (repo-daemon-dir repo))))
    (("disable" . _)
     (show-help #:quit? #t #:section "disable" #:abort "One argument required"))
    (("run" name cmd . args)
     (let ((repo (config-file-repo config name)))
       (unless (config-file-repo config name)
         (abort "'~a' is not a registered IPFS repo" name))
       (ipfs-run repo cmd args)))
    (("run" . _)
     (show-help #:quit? #t #:section "run"
                #:abort "Wrong number of arguments"))
    (("do" cmd . args)
     (for-each
       (lambda (r) (ipfs-run r cmd args))
       (config-file-repos config)))
    (("do" . _)
     (show-help #:quit? #t #:section "do"
                #:abort "Wrong number of arguments"))
    (("apply")
     (for-each
       ipfs-set-ports
       (config-file-repos config)))
    (("apply" . _)
     (show-help #:quit? #t #:section "apply"
                #:abort "Too many arguments"))
    (_ (show-help #:quit? #t #:section #t))))

(define (main raw-args)
  (let* ((opts (getargs raw-args))
         (arg-repo-dir (option-ref opts 'repo-dir #f))
         (arg-daemon-dir (option-ref opts 'daemon-dir #f))
         (arg-block-dir (option-ref opts 'block-dir #f))
         (config-file (config-file-path opts))
         (config (or
                   (and config-file
                      (file-exists? config-file)
                      (read-config-file config-file))
                   (config-file-contents
                     '()
                     #:daemon-dir #f
                     #:repo-dir #f
                     #:block-dir #f))))
    (parameterize
      ((*dry-run* (option-ref opts 'dry-run #f))
       (*quiet* (option-ref opts 'quiet #f))
       (*force* (option-ref opts 'force #f)))
      (ipfsmgr
        (option-ref opts '() #f)
        #:config config
        #:config-file config-file
        #:arg-repo-dir arg-repo-dir
        #:arg-daemon-dir arg-daemon-dir
        #:arg-block-dir arg-block-dir
        #:cfg-repo-dir
        (apply-or car (config-file-get config '(config repo-dir)))
        #:cfg-daemon-dir
        (apply-or car (config-file-get config '(config daemon-dir)))
        #:cfg-block-dir
        (apply-or car (config-file-get config '(config block-dir)))
        #:arg-no-dedup (option-ref opts 'no-dedup #f)
        #:help? (option-ref opts 'help #f)))))

