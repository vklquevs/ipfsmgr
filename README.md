# ipfsmgr

A program to manage multiple IPFS nodes.
Create nodes,
start and stop ipfs daemons,
run a single command over multiple nodes,
and slightly more!

## Motivation 

At least for go-ipfs 0.4.2, an IPFS node is limited to
a single mutable reference (i.e. `/ipns/peer-id`).
This is slightly limiting for e.g. websites with multiple subdomains
(using DNS TXT records to point to IPNS keys)
or anything else where you might want more than one mutable IPNS reference.

The purpose of `ipfsmgr` is to ease the administrative burden of taking
care of a collection of nodes.

## Features

 - `ipfsmgr init` - Set up a new node and accompanying daemon
 - `ipfsmgr ls` - List all managed nodes, their statuses and IPNS addresses
 - `ipfsmgr enable`, `ipfsmgr disable` - Start and stop `ipfs daemon` instances
 - `ipfsmgr run`, `ipfsmgr do` - Run an `ipfs` command
 on the specified node (`run`), or on all nodes (`do`)

Other bits:

 - Shares blocks between nodes to save disk space
 - Takes care of clashing API ports automatically

Future:

 - Add an already-existing node
 - Forget and/or delete nodes
 - Configure shortcuts to oft-used ipfs subcommands?

## Installation

Run `make`, then put the resulting file `ipfsmgr`
in your `$PATH`.

Requirements:

 - GNU Guile (tested on 2.0.9, 2.0.11)
 - [daemontools](http://cr.yp.to/daemontools.html) or compatible
 (see footnotes for the definition of compatibility)

## Invocation

The program should be self-explanatory enough
for new users to get going fairly quickly. If not, that's probably a bug.

Run without arguments to view command-line flags and subcommands;
use `ipfsmgr [subcommand] -h` to see more detailed help.

## Footnotes

For the purposes of `ipfsmgr`,
a system compatible with daemontools has the following properties:

 - The system treats a directory containing an executable file, named `run`,
 as a daemon.
 - The system starts a daemon
 iff its directory does not contain a file named `down`.
 - The system responds to a program named `svc` (installed in your `PATH`)
 where:
 - * `svc -u daemon-directory` issues a request to start the daemon
 - * `svc -d daemon-directory` issues a request to stop the daemon

## License

GPLv3

