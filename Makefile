
MODULES := prelude repo daemontools ipfs config program main

SCMFILES := $(patsubst %,src/%.scm,${MODULES})

all: ipfsmgr
	

ipfsmgr: ${SCMFILES}
	cat $^ > $@
	chmod +x $@

%.scm:
	@true

ipfsmgr.debug: ${SCMFILES}
	@echo -e "#!/usr/bin/guile \\ \n--fresh-auto-compile -e main -s\n!#\n" > $@
	@for i in ${SCMFILES}; do echo -e "(include \"$$i\")" >> $@; done
	@chmod +x $@


