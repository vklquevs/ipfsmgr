#!/usr/bin/guile \
--no-auto-compile -s
!#

(include "ipfsmgr.debug")
(use-modules
  (srfi srfi-64))

(test-begin "config")
(test-eq '((a b) (e (f g)))
         (config-file-set '((a b) (e)) '(e f) '(g)))
(define cfg-1
  '((config
      (daemon-dir "./daemon")
      (repo-dir "./repo")
      (block-dir "./blocks"))
    (repos)))
(test-eq
  '((config
      (daemon-dir "./daemon")
      (repo-dir "./repo")
      (block-dir "./blocks"))
    (repos
      (a (enabled #f))))
  (config-file-set cfg-1 '(repos a) '((enabled #f))))
(test-end "config")

